

CREATE TABLE `LIBRARY` (
  `library_id` int not null,
  `library_name` varchar(45) not null,
  `city` varchar(40) not null,
  PRIMARY KEY (`library_id`)
);

CREATE TABLE `USER` (
  `user_id` int not null,
  `given_name` varchar(20) ,
  `family_name` varchar(20) ,
  `email` varchar(45)  not null,
  `password` varchar(50) not null,
  PRIMARY KEY (`user_id`)
);

CREATE TABLE `LIBRARY_USER` (
  `library_id` int not null,
  `user_id` int not null,
  FOREIGN KEY (`library_id`) REFERENCES `LIBRARY`(`library_id`),
  FOREIGN KEY (`user_id`) REFERENCES `USER`(`user_id`)
);

CREATE TABLE `STATUS` (
  `status_id` int not null,
  `status` varchar(20) ,
  PRIMARY KEY (`status_id`)
);

CREATE TABLE `BOOK` (
  `book_id` int not null,
  `book_name` varchar(40) not null,
  `release_date` date,
  `status_id` int not null,
  PRIMARY KEY (`book_id`),
  FOREIGN KEY (`status_id`) REFERENCES `STATUS`(`status_id`)
);

CREATE TABLE `SECTION` (
  `section_id` int not null,
  `section` varchar(30) not null,
  `floor` varchar(10) not null,
  PRIMARY KEY (`section_id`)
);

CREATE TABLE `BOOK_SECTION` (
  `book_id` int not null,
  `section_id` int not null,
  FOREIGN KEY (`book_id`) REFERENCES `BOOK`(`book_id`),
  FOREIGN KEY (`section_id`) REFERENCES `SECTION`(`section_id`)
);

CREATE TABLE `TYPE` (
  `type_id` int not null,
  `type` varchar(20) not null,
  PRIMARY KEY (`type_id`)
);

CREATE TABLE `BOOK_TYPE` (
  `book_id` int not null,
  `type_id` int not null,
  FOREIGN KEY (`book_id`) REFERENCES `BOOK`(`book_id`),
  FOREIGN KEY (`type_id`) REFERENCES `TYPE`(`type_id`)
);

CREATE TABLE `MAILING_ADDRESS` (
  `address_id` int not null,
  `city` varchar(40) not null,
  `street` varchar(40) not null,
  `house_number` varchar(10),
  `zip_code` varchar(10),
  PRIMARY KEY (`address_id`)
);

CREATE TABLE `READER` (
  `reader_id` int not null,
  `given_name` varchar(20) not null,
  `family_name` varchar(20) not null,
  `birth_date` date,
  `email` varchar(45) not null,
  `phone_number` varchar(20),
  `address_id` int not null,
  `start_reg` date not null,
  `end_reg` date not null,
  PRIMARY KEY (`reader_id`),
  FOREIGN KEY (`address_id`) REFERENCES `MAILING_ADDRESS`(`address_id`)
);

CREATE TABLE `READER_LIBRARY` (
  `reader_id` int not null,
  `library_id` int not null,
  FOREIGN KEY (`reader_id`) REFERENCES `READER`(`reader_id`),
  FOREIGN KEY (`library_id`) REFERENCES `LIBRARY`(`library_id`)
);


CREATE TABLE `LIBRARY_BOOK` (
  `library_id` int not null,
  `book_id` int not null,
  FOREIGN KEY (`library_id`) REFERENCES `LIBRARY`(`library_id`),
  FOREIGN KEY (`book_id`) REFERENCES `BOOK`(`book_id`)
);

CREATE TABLE `AUTHOR` (
  `author_id` int not null,
  `given_name` varchar(20) not null,
  `family_name` varchar(20) not null,
  `birth_date` date,
  `description` varchar(1000),
  PRIMARY KEY (`author_id`)
);

CREATE TABLE `SCHOOL` (
  `school_id` int not null,
  `school_name` varchar(45) not null,
  `city` varchar(40) not null,
  PRIMARY KEY (`school_id`)
);

CREATE TABLE `READER_SCHOOL` (
  `reader_id` int not null,
  `school_id` int not null,
  FOREIGN KEY (`school_id`) REFERENCES `SCHOOL`(`school_id`),
  FOREIGN KEY (`reader_id`) REFERENCES `READER`(`reader_id`)
);

CREATE TABLE `READER_BOOK` (
  `reader_id` int not null,
  `book_id` int not null,
  FOREIGN KEY (`reader_id`) REFERENCES `READER`(`reader_id`),
  FOREIGN KEY (`book_id`) REFERENCES `BOOK`(`book_id`)
);

CREATE TABLE `AUTHOR_BOOK` (
  `author_id` int not null,
  `book_id` int not null,
  FOREIGN KEY (`author_id`) REFERENCES `AUTHOR`(`author_id`),
  FOREIGN KEY (`book_id`) REFERENCES `BOOK`(`book_id`)
);

