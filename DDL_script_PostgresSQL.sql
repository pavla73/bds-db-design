

CREATE TABLE if not exists "LIBRARY" (
  "library_id" serial not null,
  "library_name" varchar(45) not null,
  "city" varchar(40) not null,
  PRIMARY KEY ("library_id")
);

CREATE TABLE if not exists "USER" (
  "user_id" serial not null,
  "given_name" varchar(20) ,
  "family_name" varchar(20) ,
  "email" varchar(45)  not null,
  "password" varchar(50) not null,
  PRIMARY KEY ("user_id")
);

CREATE TABLE if not exists "LIBRARY_USER" (
  "library_id" integer not null,
  "user_id" integer not null,
  CONSTRAINT "FK_LIBRARY_USER.library_id"
    FOREIGN KEY ("library_id")
      REFERENCES "LIBRARY"("library_id"),
  CONSTRAINT "FK_LIBRARY_USER.user_id"
    FOREIGN KEY ("user_id")
      REFERENCES "USER"("user_id")
);

CREATE TABLE if not exists "STATUS" (
  "status_id" serial not null,
  "status" varchar(20) ,
  PRIMARY KEY ("status_id")
);

CREATE TABLE if not exists "BOOK" (
  "book_id" serial not null,
  "book_name" varchar(40) not null,
  "release_date" date,
  "status_id" integer not null,
  PRIMARY KEY ("book_id"),
  CONSTRAINT "FK_BOOK.status_id"
    FOREIGN KEY ("status_id")
      REFERENCES "STATUS"("status_id")
);

CREATE TABLE if not exists "SECTION" (
  "section_id" serial not null,
  "section" varchar(30) not null,
  "floor" varchar(10) not null,
  PRIMARY KEY ("section_id")
);

CREATE TABLE if not exists "BOOK_SECTION" (
  "book_id" integer not null,
  "section_id" integer not null,
  CONSTRAINT "FK_BOOK_SECTION.book_id"
    FOREIGN KEY ("book_id")
      REFERENCES "BOOK"("book_id"),
  CONSTRAINT "FK_BOOK_SECTION.section_id"
    FOREIGN KEY ("section_id")
      REFERENCES "SECTION"("section_id")
);

CREATE TABLE if not exists "TYPE" (
  "type_id" serial not null,
  "type" varchar(20) not null,
  PRIMARY KEY ("type_id")
);

CREATE TABLE if not exists "BOOK_TYPE" (
  "book_id" integer not null,
  "type_id" integer not null,
  CONSTRAINT "FK_BOOK_TYPE.book_id"
    FOREIGN KEY ("book_id")
      REFERENCES "BOOK"("book_id"),
  CONSTRAINT "FK_BOOK_TYPE.type_id"
    FOREIGN KEY ("type_id")
      REFERENCES "TYPE"("type_id")
);

CREATE TABLE if not exists "MAILING_ADDRESS" (
  "address_id" serial not null,
  "city" varchar(40) not null,
  "street" varchar(40) not null,
  "house_number" varchar(10),
  "zip_code" varchar(10),
  PRIMARY KEY ("address_id")
);

CREATE TABLE if not exists "READER" (
  "reader_id" serial not null,
  "given_name" varchar(20) not null,
  "family_name" varchar(20) not null,
  "birth_date" date,
  "email" varchar(45) not null,
  "address_id" integer not null,
  "phone_number" varchar(20),
  "start_reg" date not null,
  "end_reg" date not null,
  PRIMARY KEY ("reader_id"),
  CONSTRAINT "FK_READER.address_id"
    FOREIGN KEY ("address_id")
      REFERENCES "MAILING_ADDRESS"("address_id")
);

CREATE TABLE if not exists "READER_LIBRARY" (
  "reader_id" integer not null,
  "library_id" integer not null,
  CONSTRAINT "FK_READER_LIBRARY.reader_id"
    FOREIGN KEY ("reader_id")
      REFERENCES "READER"("reader_id"),
  CONSTRAINT "FK_READER_LIBRARY.library_id"
    FOREIGN KEY ("library_id")
      REFERENCES "LIBRARY"("library_id")
);


CREATE TABLE if not exists "LIBRARY_BOOK" (
  "library_id" integer not null,
  "book_id" integer not null,
  CONSTRAINT "FK_LIBRARY_BOOK.library_id"
    FOREIGN KEY ("library_id")
      REFERENCES "LIBRARY"("library_id"),
  CONSTRAINT "FK_LIBRARY_BOOK.book_id"
    FOREIGN KEY ("book_id")
      REFERENCES "BOOK"("book_id")

);

CREATE TABLE if not exists "AUTHOR" (
  "author_id" serial not null,
  "given_name" varchar(20) not null,
  "family_name" varchar(20) not null,
  "birth_date" date,
  "description" varchar(1000),
  PRIMARY KEY ("author_id")
);

CREATE TABLE if not exists "SCHOOL" (
  "school_id" serial not null,
  "school_name" varchar(45) not null,
  "city" varchar(40) not null,
  PRIMARY KEY ("school_id")
);

CREATE TABLE if not exists "READER_SCHOOL" (
  "reader_id" integer not null,
  "school_id" integer not null,
  CONSTRAINT "FK_READER_SCHOOL.school_id"
    FOREIGN KEY ("school_id")
      REFERENCES "SCHOOL"("school_id"),
  CONSTRAINT "FK_READER_SCHOOL.reader_id"
    FOREIGN KEY ("reader_id")
      REFERENCES "READER"("reader_id")
);


CREATE TABLE if not exists "READER_BOOK" (
  "reader_id" integer not null,
  "book_id" integer not null,
  CONSTRAINT "FK_READER_BOOK.reader_id"
    FOREIGN KEY ("reader_id")
      REFERENCES "READER"("reader_id"),
  CONSTRAINT "FK_READER_BOOK.book_id"
    FOREIGN KEY ("book_id")
      REFERENCES "BOOK"("book_id")
);

CREATE TABLE if not exists "AUTHOR_BOOK" (
  "author_id" integer not null,
  "book_id" integer not null,
  CONSTRAINT "FK_AUTHOR_BOOK.author_id"
    FOREIGN KEY ("author_id")
      REFERENCES "AUTHOR"("author_id"),
  CONSTRAINT "FK_AUTHOR_BOOK.book_id"
    FOREIGN KEY ("book_id")
      REFERENCES "BOOK"("book_id")
);